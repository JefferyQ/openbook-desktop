use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;

use super::DecodeError;
use crate::util;
use crate::util::paths;

use flutter_engine::{
    channel::{Channel, StandardMethodChannel},
    codec::{standard_codec::Value, MethodCallResult},
    FlutterEngineInner, PlatformMessage, Plugin, PluginRegistry, Window,
};
use log::{debug, info, trace, warn};
use sqlite::Connection;

const CHANNEL_NAME: &str = "com.tekartik.sqflite";

plugin_args! {
    OpenDatabaseArgs,
    path: String, "path", Value::String(v) => v.clone();
    #[optional=true]
    read_only: Option<bool>, "readOnly", Value::Boolean(b) => Some(b.clone());
}

plugin_args! {
    CloseDatabaseArgs,
    id: i32, "id", Value::I32(i) => i.clone();
}

plugin_args! {
    ExecuteArgs,
    id: i32, "id", Value::I32(i) => i.clone();
    sql: String, "sql", Value::String(s) => s.clone();
    args: Option<Vec<Value>>, "arguments", Value::List(l) => Some(util::clone_value_list(l)), Value::Null => None;
}

plugin_args! {
    QueryArgs,
    id: i32, "id", Value::I32(i) => i.clone();
    sql: String, "sql", Value::String(s) => s.clone();
    args: Option<Vec<Value>>, "arguments", Value::List(l) => Some(util::clone_value_list(l)), Value::Null => None;
}

plugin_args! {
    UpdateArgs,
    id: i32, "id", Value::I32(i) => i.clone();
    sql: String, "sql", Value::String(s) => s.clone();
    args: Option<Vec<Value>>, "arguments", Value::List(l) => Some(util::clone_value_list(l)), Value::Null => None;
}

plugin_args! {
    InsertArgs,
    id: i32, "id", Value::I32(i) => i.clone();
    sql: String, "sql", Value::String(s) => s.clone();
    args: Option<Vec<Value>>, "arguments", Value::List(l) => Some(util::clone_value_list(l)), Value::Null => None;
}

pub struct SqflitePlugin {
    channel: StandardMethodChannel,
    open_databases: HashMap<i32, Connection>,
    current_database_id: i32,
}

impl SqflitePlugin {
    pub fn new() -> Self {
        Self {
            channel: StandardMethodChannel::new(CHANNEL_NAME),
            open_databases: HashMap::new(),
            current_database_id: 0,
        }
    }

    fn get_databases_path(&self) -> MethodCallResult<Value> {
        match paths::get_data_dir() {
            Some(dir) => MethodCallResult::Ok(Value::String(dir.to_string_lossy().into_owned())),
            None => crate::util::error_result(),
        }
    }

    fn open_database(&mut self, args: OpenDatabaseArgs) -> MethodCallResult<Value> {
        let path = PathBuf::from(args.path);
        let db = match args.read_only {
            Some(true) => sqlite::Connection::open_readonly(path),
            _ => sqlite::Connection::open(path),
        };
        let db = match db {
            Ok(db) => db,
            _ => return util::error_result(),
        };

        let id = self.current_database_id;
        self.current_database_id += 1;
        self.open_databases.insert(id, db);

        MethodCallResult::Ok(Value::I32(id.into()))
    }

    fn close_database(&mut self, args: CloseDatabaseArgs) -> MethodCallResult<Value> {
        self.open_databases.remove(&args.id);
        MethodCallResult::Ok(Value::Null)
    }

    fn execute(&self, args: ExecuteArgs) -> MethodCallResult<Value> {
        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return util::error_result(),
        };

        let mut statement = match db.prepare(&args.sql) {
            Ok(statement) => statement,
            _ => {
                warn!("Failed to create statement from SQL \"{}\"", &args.sql);
                return util::error_result();
            }
        };
        if let Some(sql_args) = args.args {
            if bind_values_to_statement(&mut statement, &sql_args).is_err() {
                warn!("Failed to bind values in SQL statement \"{}\"", args.sql);
                return util::error_result();
            }
        }
        while let sqlite::State::Row = match statement.next() {
            Ok(s) => s,
            _ => return util::error_result(),
        } {}
        MethodCallResult::Ok(Value::Null)
    }

    fn query(&self, args: QueryArgs) -> MethodCallResult<Value> {
        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return util::error_result(),
        };

        let mut statement = match db.prepare(&args.sql) {
            Ok(statement) => statement,
            _ => {
                warn!("Failed to create statement from SQL \"{}\"", &args.sql);
                return util::error_result();
            }
        };
        if let Some(sql_args) = args.args {
            if bind_values_to_statement(&mut statement, &sql_args).is_err() {
                warn!("Failed to bind values in SQL statement \"{}\"", args.sql);
                return util::error_result();
            }
        }

        let column_count = statement.count();
        let column_names = statement
            .names()
            .into_iter()
            .map(|s| Value::String(String::from(s)))
            .collect();

        //TODO: implement options call and returning a list of maps instead of a map with a list of lists

        let mut results_map = HashMap::new();
        results_map.insert(
            Value::String(String::from("columns")),
            Value::List(column_names),
        );
        let mut result_rows = Vec::new();

        while let sqlite::State::Row = match statement.next() {
            Ok(s) => s,
            _ => return util::error_result(),
        } {
            result_rows.push(match statement_row_to_vec(&statement, column_count) {
                Ok(v) => Value::List(v),
                _ => return util::error_result(),
            });
        }
        results_map.insert(
            Value::String(String::from("rows")),
            Value::List(result_rows),
        );

        MethodCallResult::Ok(Value::Map(results_map))
    }

    fn update(&mut self, args: UpdateArgs) -> MethodCallResult<Value> {
        let result = self.execute(ExecuteArgs {
            id: args.id,
            sql: args.sql.clone(),
            args: args.args,
        });
        match &result {
            MethodCallResult::Err { .. } => return result,
            _ => (),
        }

        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return util::error_result(),
        };
        let mut statement = match db.prepare("SELECT changes()") {
            Ok(statement) => statement,
            _ => {
                warn!("Failed to create statement for update");
                return util::error_result();
            }
        };
        match statement.next() {
            Ok(sqlite::State::Row) => match statement.read::<i64>(0) {
                Ok(changes) => MethodCallResult::Ok(Value::I64(changes)),
                _ => {
                    warn!("Reading changes for update didn't return an integer");
                    MethodCallResult::Ok(Value::Null)
                }
            },
            _ => {
                warn!("Failed to read changes for update");
                MethodCallResult::Ok(Value::Null)
            }
        }
    }

    fn insert(&mut self, args: InsertArgs) -> MethodCallResult<Value> {
        let result = self.execute(ExecuteArgs {
            id: args.id,
            sql: args.sql.clone(),
            args: args.args,
        });
        match &result {
            MethodCallResult::Err { .. } => return result,
            _ => (),
        }

        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return util::error_result(),
        };
        let mut statement = match db.prepare("SELECT changes(), last_insert_rowid()") {
            Ok(statement) => statement,
            _ => {
                warn!("Failed to create statement for insert");
                return util::error_result();
            }
        };
        match statement.next() {
            Ok(sqlite::State::Row) => match (statement.read::<i64>(0), statement.read::<i64>(1)) {
                (Ok(changes), Ok(rowid)) => MethodCallResult::Ok(Value::I64(rowid)),
                _ => {
                    warn!("Reading changes for insert didn't return an integer");
                    MethodCallResult::Ok(Value::Null)
                }
            },
            _ => {
                warn!("Failed to read changes for insert");
                MethodCallResult::Ok(Value::Null)
            }
        }
    }
}

impl Plugin for SqflitePlugin {
    fn init_channel(&self, registry: &PluginRegistry) -> &str {
        self.channel.init(registry);
        CHANNEL_NAME
    }

    fn handle(
        &mut self,
        msg: &PlatformMessage,
        _engine: Arc<FlutterEngineInner>,
        _window: &mut Window,
    ) {
        let decoded = self.channel.decode_method_call(msg);

        debug!(
            "Got method call {} with args: {}",
            decoded.method,
            super::debug_print_args(&decoded.args)
        );
        match decoded.method.as_str() {
            "getPlatformVersion" => {}
            "debugMode" => {}
            "closeDatabase" => {
                let response = match CloseDatabaseArgs::from_value(decoded.args) {
                    Ok(args) => self.close_database(args),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "query" => {
                let response = match QueryArgs::from_value(decoded.args) {
                    Ok(args) => self.query(args),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "insert" => {
                let response = match InsertArgs::from_value(decoded.args) {
                    Ok(args) => self.insert(args),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "update" => {
                let response = match UpdateArgs::from_value(decoded.args) {
                    Ok(args) => self.update(args),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "execute" => {
                let response = match ExecuteArgs::from_value(decoded.args) {
                    Ok(args) => self.execute(args),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "openDatabase" => {
                let response = match OpenDatabaseArgs::from_value(decoded.args) {
                    Ok(args) => self.open_database(args),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "batch" => {}
            "options" => {}
            "getDatabasesPath" => {
                let response = self.get_databases_path();
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            _ => {}
        }
    }
}

enum BindError {
    SqlError(sqlite::Error),
    TypeError,
}

fn bind_values_to_statement(
    statement: &mut sqlite::Statement,
    values: &Vec<Value>,
) -> Result<(), BindError> {
    let mut index = 1;
    for value in values.iter() {
        match value {
            Value::Null => statement.bind(index, ()).map_err(BindError::SqlError),
            Value::String(s) => statement
                .bind(index, s.as_str())
                .map_err(BindError::SqlError),
            Value::I64(i) => statement
                .bind(index, i.clone())
                .map_err(BindError::SqlError),
            Value::I32(i) => statement
                .bind(index, i.clone() as i64)
                .map_err(BindError::SqlError),
            Value::F64(f) => statement
                .bind(index, f.clone())
                .map_err(BindError::SqlError),
            Value::U8List(l) => statement
                .bind::<&[u8]>(index, l)
                .map_err(BindError::SqlError),
            _ => Err(BindError::TypeError),
        }?;
        index += 1;
    }
    Ok(())
}

fn statement_row_to_vec(
    statement: &sqlite::Statement,
    column_count: usize,
) -> sqlite::Result<Vec<Value>> {
    let mut vec = Vec::with_capacity(column_count);

    for i in 0..column_count {
        vec.push(match statement.kind(i) {
            sqlite::Type::Null => Value::Null,
            sqlite::Type::String => Value::String(statement.read(i)?),
            sqlite::Type::Integer => Value::I64(statement.read(i)?),
            sqlite::Type::Float => Value::F64(statement.read(i)?),
            sqlite::Type::Binary => Value::U8List(statement.read(i)?),
        });
    }

    Ok(vec)
}

fn statement_row_to_map(
    statement: &sqlite::Statement,
    column_count: usize,
) -> sqlite::Result<HashMap<Value, Value>> {
    let mut map = HashMap::with_capacity(column_count);
    let column_names = statement.names();

    for i in 0..column_count {
        map.insert(
            Value::String(String::from(column_names.get(i).unwrap().clone())),
            match statement.kind(i) {
                sqlite::Type::Null => Value::Null,
                sqlite::Type::String => Value::String(statement.read(i)?),
                sqlite::Type::Integer => Value::I64(statement.read(i)?),
                sqlite::Type::Float => Value::F64(statement.read(i)?),
                sqlite::Type::Binary => Value::U8List(statement.read(i)?),
            },
        );
    }

    Ok(map)
}
