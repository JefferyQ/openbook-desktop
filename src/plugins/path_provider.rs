use std::sync::Arc;
use std::{fs, path::PathBuf};

use crate::util::paths;

use flutter_engine::{
    channel::{Channel, StandardMethodChannel},
    codec::{standard_codec::Value, MethodCallResult},
    FlutterEngineInner, PlatformMessage, Plugin, PluginRegistry, Window,
};
use log::{debug, info, trace};

const CHANNEL_NAME: &str = "plugins.flutter.io/path_provider";

pub struct PathProviderPlugin {
    channel: StandardMethodChannel,
}

impl PathProviderPlugin {
    pub fn new() -> Self {
        Self {
            channel: StandardMethodChannel::new(CHANNEL_NAME),
        }
    }

    fn get_directory_result(&self, dir: Option<PathBuf>) -> MethodCallResult<Value> {
        match dir {
            Some(dir) => MethodCallResult::Ok(Value::String(dir.to_string_lossy().into_owned())),
            None => MethodCallResult::Err {
                details: Value::Null,
                code: String::from(""),
                message: String::from(""),
            },
        }
    }

    fn get_temporary_directory(&self) -> MethodCallResult<Value> {
        self.get_directory_result(paths::get_cache_dir())
    }

    fn get_application_documents_directory(&self) -> MethodCallResult<Value> {
        self.get_directory_result(paths::get_data_dir())
    }

    fn get_storage_directory(&self) -> MethodCallResult<Value> {
        self.get_directory_result(paths::get_home_dir())
    }
}

impl Plugin for PathProviderPlugin {
    fn init_channel(&self, registry: &PluginRegistry) -> &str {
        self.channel.init(registry);
        CHANNEL_NAME
    }

    fn handle(
        &mut self,
        msg: &PlatformMessage,
        _engine: Arc<FlutterEngineInner>,
        _window: &mut Window,
    ) {
        let decoded = self.channel.decode_method_call(msg);

        debug!(
            "Got method call {} with args: {}",
            decoded.method,
            super::debug_print_args(&decoded.args)
        );
        match decoded.method.as_str() {
            "getTemporaryDirectory" => {
                let response = self.get_temporary_directory();
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "getApplicationDocumentsDirectory" => {
                let response = self.get_application_documents_directory();
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "getStorageDirectory" => {
                let response = self.get_storage_directory();
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            _ => (),
        }
    }
}
