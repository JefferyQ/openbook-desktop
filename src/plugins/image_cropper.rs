use std::path::PathBuf;
use std::sync::Arc;

use super::DecodeError;
use crate::util;

use flutter_engine::{
    channel::{Channel, StandardMethodChannel},
    codec::{standard_codec::Value, MethodCallResult},
    FlutterEngineInner, PlatformMessage, Plugin, PluginRegistry, Window,
};
use log::{debug, info, trace, warn};

const CHANNEL_NAME: &str = "plugins.hunghd.vn/image_cropper";

plugin_args! {
    CropImageArgs,
    source_path: String, "source_path", Value::String(s) => s.clone();
    max_width: Option<i32>, "max_width", Value::I32(i) => Some(*i), Value::Null => None;
    max_height: Option<i32>, "max_height", Value::I32(i) => Some(*i), Value::Null => None;
    ratio_x: Option<f64>, "ratio_x", Value::F64(f) => Some(*f), Value::Null => None;
    ratio_y: Option<f64>, "ratio_y", Value::F64(f) => Some(*f), Value::Null => None;
    toolbar_title: String, "toolbar_title", Value::String(s) => s.clone();
    toolbar_color: i64, "toolbar_color", Value::I64(i) => *i;
    circle_shape: bool, "circle_shape", Value::Boolean(b) => *b;
}

pub struct ImageCropperPlugin {
    channel: StandardMethodChannel,
}

impl ImageCropperPlugin {
    pub fn new() -> Self {
        Self {
            channel: StandardMethodChannel::new(CHANNEL_NAME),
        }
    }

    fn crop_image(&self, args: CropImageArgs) -> MethodCallResult<Value> {
        //TODO: actually implement cropping... or at least respect max_width and max_height
        MethodCallResult::Ok(Value::String(args.source_path))
    }
}

impl Plugin for ImageCropperPlugin {
    fn init_channel(&self, registry: &PluginRegistry) -> &str {
        self.channel.init(registry);
        CHANNEL_NAME
    }

    fn handle(
        &mut self,
        msg: &PlatformMessage,
        _engine: Arc<FlutterEngineInner>,
        _window: &mut Window,
    ) {
        let decoded = self.channel.decode_method_call(msg);

        debug!(
            "Got method call {} with args: {}",
            decoded.method,
            super::debug_print_args(&decoded.args)
        );

        match decoded.method.as_str() {
            "cropImage" => {
                let response = match CropImageArgs::from_value(decoded.args) {
                    Ok(args) => self.crop_image(args),
                    _ => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            method => {
                warn!("Unknown method {} called", method);
            }
        }
    }
}
