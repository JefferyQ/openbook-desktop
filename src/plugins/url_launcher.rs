use std::path::PathBuf;
use std::sync::Arc;

use super::DecodeError;
use crate::util;

use flutter_engine::{
    channel::{Channel, StandardMethodChannel},
    codec::{standard_codec::Value, MethodCallResult},
    FlutterEngineInner, PlatformMessage, Plugin, PluginRegistry, Window,
};
use log::{debug, info, trace};

const CHANNEL_NAME: &str = "plugins.flutter.io/url_launcher";

plugin_args! {
    UrlArgs,
    url: String, "url", Value::String(v) => v.clone();
}

pub struct UrlLauncherPlugin {
    channel: StandardMethodChannel,
}

impl UrlLauncherPlugin {
    pub fn new() -> Self {
        Self {
            channel: StandardMethodChannel::new(CHANNEL_NAME),
        }
    }
}

impl Plugin for UrlLauncherPlugin {
    fn init_channel(&self, registry: &PluginRegistry) -> &str {
        self.channel.init(registry);
        CHANNEL_NAME
    }

    fn handle(
        &mut self,
        msg: &PlatformMessage,
        _engine: Arc<FlutterEngineInner>,
        _window: &mut Window,
    ) {
        let decoded = self.channel.decode_method_call(msg);

        debug!(
            "Got method call {} with args: {}",
            decoded.method,
            super::debug_print_args(&decoded.args)
        );
        let url = match UrlArgs::from_value(decoded.args) {
            Ok(args) => args.url,
            _ => {
                self.channel
                    .send_method_call_response(msg.response_handle, util::error_result());
                return;
            }
        };
        match decoded.method.as_str() {
            "canLaunch" => {
                self.channel.send_method_call_response(
                    msg.response_handle,
                    MethodCallResult::Ok(Value::Boolean(true)),
                );
            }
            "launch" => {
                let response = match webbrowser::open(url.as_str()) {
                    Ok(_) => MethodCallResult::Ok(Value::Null),
                    Err(_) => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "closeWebView" => {
                self.channel.send_method_call_response(
                    msg.response_handle,
                    MethodCallResult::Ok(Value::Null),
                );
            }
            _ => (),
        }
    }
}
