use std::path::PathBuf;
use std::sync::Arc;

use super::DecodeError;
use crate::util;

use flutter_engine::{
    channel::{Channel, StandardMethodChannel},
    codec::{standard_codec::Value, MethodCallResult},
    FlutterEngineInner, PlatformMessage, Plugin, PluginRegistry, Window,
};
use log::{debug, info, trace, warn};

const CHANNEL_NAME: &str = "plugins.flutter.io/image_picker";

enum ImageSource {
    Gallery,
    Camera,
}

plugin_args! {
    PickImageArgs,
    source: Option<ImageSource>, "source", Value::I32(i) => match i {
        0 => Some(ImageSource::Camera),
        1 => Some(ImageSource::Gallery),
        _ => None
    };
    max_width: Option<f64>, "maxWidth", Value::F64(f) => Some(*f), Value::Null => None;
    max_height: Option<f64>, "maxHeight", Value::F64(f) => Some(*f), Value::Null => None;
}

pub struct ImagePickerPlugin {
    channel: StandardMethodChannel,
}

impl ImagePickerPlugin {
    pub fn new() -> Self {
        Self {
            channel: StandardMethodChannel::new(CHANNEL_NAME),
        }
    }

    fn pick_image(&self, args: PickImageArgs) -> MethodCallResult<Value> {
        let path = match util::paths::get_home_dir() {
            Some(path) => match path.to_str() {
                Some(path) => Some(path.to_owned()),
                None => None,
            },
            None => None,
        };
        let path = match path {
            Some(path) => path,
            None => return util::error_result(),
        };

        let filter = Some((&["*.jpg", "*.png", "*.gif", "*.bmp"][0..], "Image files"));
        let result = tinyfiledialogs::open_file_dialog("Openbook", path.as_str(), filter);
        match result {
            Some(path) => MethodCallResult::Ok(Value::String(path)), //TODO: resize image to max_width / max_height if given
            None => MethodCallResult::Ok(Value::Null),
        }
    }
}

impl Plugin for ImagePickerPlugin {
    fn init_channel(&self, registry: &PluginRegistry) -> &str {
        self.channel.init(registry);
        CHANNEL_NAME
    }

    fn handle(
        &mut self,
        msg: &PlatformMessage,
        _engine: Arc<FlutterEngineInner>,
        _window: &mut Window,
    ) {
        let decoded = self.channel.decode_method_call(msg);

        debug!(
            "Got method call {} with args: {}",
            decoded.method,
            super::debug_print_args(&decoded.args)
        );
        let args = PickImageArgs::from_value(decoded.args).ok();

        match decoded.method.as_str() {
            "pickImage" => {
                let response = match args {
                    Some(args) => self.pick_image(args),
                    None => util::error_result(),
                };
                self.channel
                    .send_method_call_response(msg.response_handle, response);
            }
            "pickVideo" => {
                warn!("Method pickVideo unimplemented");
            }
            method => {
                warn!("Unknown method {} called", method);
            }
        }
    }
}
