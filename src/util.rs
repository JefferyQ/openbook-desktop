use flutter_engine::codec::{standard_codec::Value, MethodCallResult};
use std::collections::HashMap;

pub mod paths;

pub fn error_result() -> MethodCallResult<Value> {
    MethodCallResult::Err {
        details: Value::Null,
        code: String::from(""),
        message: String::from(""),
    }
}

pub fn clone_value(value: &Value) -> Value {
    match value {
        Value::String(s) => Value::String(s.clone()),
        Value::Boolean(b) => Value::Boolean(b.clone()),
        Value::F64(f) => Value::F64(f.clone()),
        Value::I32(i) => Value::I32(i.clone()),
        Value::I64(i) => Value::I64(i.clone()),
        Value::LargeInt => Value::LargeInt,
        Value::Null => Value::Null,
        Value::F64List(l) => Value::F64List(l.to_vec()),
        Value::I32List(l) => Value::I32List(l.to_vec()),
        Value::I64List(l) => Value::I64List(l.to_vec()),
        Value::U8List(l) => Value::U8List(l.to_vec()),
        Value::List(l) => Value::List(clone_value_list(l)),
        Value::Map(m) => Value::Map(clone_value_map(m)),
    }
}

pub fn clone_value_list(list: &Vec<Value>) -> Vec<Value> {
    let mut new_list = Vec::with_capacity(list.len());
    for v in list.iter() {
        new_list.push(clone_value(v));
    }
    new_list
}

pub fn clone_value_map(map: &HashMap<Value, Value>) -> HashMap<Value, Value> {
    let mut new_map = HashMap::with_capacity(map.len());
    for (k, v) in map.iter() {
        new_map.insert(clone_value(k), clone_value(v));
    }
    new_map
}
