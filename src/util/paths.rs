use std::{fs, path::PathBuf};

fn get_openbook_dir(dir: Option<PathBuf>, subdir: bool) -> Option<PathBuf> {
    match dir {
        Some(dir) => {
            let dir = if subdir { dir.join("openbook") } else { dir };
            match fs::create_dir_all(&dir) {
                Ok(_) => Some(dir),
                Err(_) => None,
            }
        }
        None => None,
    }
}

pub fn get_data_dir() -> Option<PathBuf> {
    get_openbook_dir(dirs::data_dir(), true)
}

pub fn get_home_dir() -> Option<PathBuf> {
    get_openbook_dir(dirs::data_dir(), false)
}

pub fn get_cache_dir() -> Option<PathBuf> {
    get_openbook_dir(dirs::cache_dir(), true)
}
