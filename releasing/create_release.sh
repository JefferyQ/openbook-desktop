#!/usr/bin/env sh

read -r -d '' PAYLOAD <<EOF
{ "name": "Release $CI_COMMIT_TAG", "tag_name": "$CI_COMMIT_TAG", "description": "Release", "assets": { "links": [
{ "name": "Linux binaries", "url": "$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_TAG/download?job=release-linux" },
{ "name": "Windows binaries", "url": "$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_TAG/download?job=release-windows" }
]}}
EOF
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $API_ACCESS_TOKEN" \
     --data "$PAYLOAD" \
     --request POST "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases"
